  /*
package cz.alej.alisma.lp;

/*
 * Tento program ma prepisovat text do Morseovy abecedy.
 *
 * Zatim ale umi jen nacitat jednotlive znaky na vstupu a
 * preskakovat tzv. bile znaky (mezery, znaky noveho radku
 * apod.).
 *
 * Doplnte implementaci metody prevedZnak, aby umela prevest
 * alespon vsechna pismena. Vyuzijte vhodnou tridu z Java
 * Collections pro ulozeni vlastniho mapovani mezi znakem
 * a jeho morseovou reprezentaci.
 *
 * Pri vstupu "Ahoj svete" ocekavame vystup
 *
 * .-/..../---/.---/.../...-/./-/./
 */

import java.util.Scanner;
import java.util.Map;
import java.util.HashMap;
import java.lang.Character;

public class Morseovka {
    private static final String ODDELOVAC = ".|\\n";
    private static final String NEZNAME = "??";
    
    private static Map<Character, String> morseovka;
    /*
    private static String prevedZnak(Character znak) {
        znak = Character.toLowerCase(znak);
        String morse = morseovka.get("znak"); 
        return morse;
    }
    
    /*private static String prevedZnak(Character znak) {
        String morse = morseovka.get("znak"); 
        return morse;
        
    }
    */
    private static String prevedZnak(Character znak) {
        znak = Character.toLowerCase(znak);
        if (Character.isWhitespace(znak)) {
            return null;  
        }else{
            String morse = morseovka.get(znak); 
            return morse;
        }
    }
    
    public static void main(String[] args) {
        morseovka = new HashMap<Character, String>();   
        morseovka.put('a', ".-");
        morseovka.put('b', "-...");
        morseovka.put('c', "-.-.");
        morseovka.put('d', "-..");
        morseovka.put('e', ".");
        morseovka.put('f', "..-.");
        morseovka.put('g', "--.");
        morseovka.put('h', "....");
        morseovka.put('i', "..");
        morseovka.put('j', ".---");
        morseovka.put('k', "-.-");
        morseovka.put('l', ".-..");
        morseovka.put('m', "--");
        morseovka.put('n', "-.");
        morseovka.put('o', "---");
        morseovka.put('p', ".--.");
        morseovka.put('q', "--.-");
        morseovka.put('r', ".-.");
        morseovka.put('s', "...");
        morseovka.put('t', "-");
        morseovka.put('u', "..-");
        morseovka.put('v', "...-");
        morseovka.put('w', ".--");
        morseovka.put('x', "-..-");
        morseovka.put('y', "-.--");
        morseovka.put('z', "--.."); 
      
        Scanner sc = new Scanner(System.in);
        sc.useDelimiter("");
        while (sc.hasNext(ODDELOVAC)) {
            char znak = sc.next(ODDELOVAC).charAt(0);
            String morse = prevedZnak(znak);
            if (morse == null) {
                continue;
            }
            System.out.printf("%s/", morse);
        }
        sc.close();
        System.out.println();
    }
 
}
//S touto metodou (nápad použití HashMap) mi pomohl Petr Novák
